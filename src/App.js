import React, { Component } from 'react';
import './App.css';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeflex/primeflex.css'
import 'primeicons/primeicons.css';

import Calculator from './components/Calculator';
import Counter from './components/Counter';
import Home from './components/Home';
import TableData from './components/Table';
import FileUpload from './components/FileUpload';

import { Route } from 'react-router-dom';


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      movies: "",
      testing: "Working"
    }
  }


  render() {
    return (
      <div className="App">

        <div>
          <Route path="/" exact component={Home} />
          <Route path="/calc" exact component={Calculator} />
          <Route path="/count" exact component={Counter} />
          <Route path="/table" exact component={TableData} />
          <Route path="/file" exact component={FileUpload} />
        </div>
      </div>
    );
  }
}

export default App;
