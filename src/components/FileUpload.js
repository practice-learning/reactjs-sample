import React, { useState } from 'react';
import { FileUpload } from 'primereact/fileupload';
import { Growl } from 'primereact/growl';

const PurchaseUploadForm = () => {
	const [growl, setGrowl] = useState("");

	const onUpload = (event) => {
		growl.show({
			severity: 'info',
			summary: 'Success',
			detail: 'File Uploaded'
		});
	}

	return (
		<div>
			<div className='content-section implementation'>
				<h3>Advanced</h3>
				<FileUpload
					mode="basic"
					name='demo[]'
					url='https://zafsiumb77.execute-api.us-east-1.amazonaws.com/dev/upload'
					onUpload={onUpload}
					multiple={true}
					accept='.xml'
					maxFileSize={1000000}
				/>
				<Growl
					ref={el => {
						setGrowl(el);
					}}
				/>


			</div>
			<div class="p-grid">
				<div class="p-col">1</div>
				<div class="p-col">2</div>
				<div class="p-col">3</div>
			</div>
		</div>
	)
}

export default PurchaseUploadForm;