import React, {Component} from 'react';
import {Button} from 'primereact/button';

const KeyPadComponent = (props) => {
    return(
            <div className="p-grid">
                 <div className="p-col-6">
                    <Button label="("  className="p-button-rounded"   onClick={ () => props.onClick("(")}/>                    
                    <Button label="CE" className="p-button-rounded"   onClick={ () => props.onClick("CE")}/>
                    <Button label=")"  className="p-button-rounded"   onClick={ () => props.onClick(")" )}/>
                    <Button label="C"  className="p-button-rounded"   onClick={ () => props.onClick("C" )}/><br/>
                </div>
                <div className="p-col-6">
                    <Button  label="1"  className="p-button-rounded"  onClick={ () => props.onClick("1")}/>
                    <Button  label="2"  className="p-button-rounded"  onClick={ () => props.onClick("2")}/>
                    <Button  label="3"  className="p-button-rounded"  onClick={ () => props.onClick("3")}/>
                    <Button  label="+"  className="p-button-rounded"  onClick={ () => props.onClick("+")}/><br/>
                </div>
                <div className="p-col-6">
                    <Button  label="4"  className="p-button-rounded"  onClick={ () => props.onClick("4")}/>
                    <Button  label="5"  className="p-button-rounded"  onClick={ () => props.onClick("5")}/>
                    <Button  label="6"  className="p-button-rounded"  onClick={ () => props.onClick("6")}/>
                    <Button  label="-"  className="p-button-rounded"  onClick={ () => props.onClick("-")}/>
                </div>
                <div className="p-col-6">
                    <Button  label="7"  className="p-button-rounded"  onClick={ () => props.onClick("7")}/>
                    <Button  label="8"  className="p-button-rounded"  onClick={ () => props.onClick("8")}/>
                    <Button  label="9"  className="p-button-rounded"  onClick={ () => props.onClick("9")}/>
                    <Button  label="*"  className="p-button-rounded"  onClick={ () => props.onClick("*")}/>
                </div>
                <div className="p-col-6">
                    <Button  label="."  className="p-button rounded"  onClick={ () => props.onClick(".")}/>
                    <Button  label="0"  className="p-button rounded"  onClick={ () => props.onClick("0")}/>
                    <Button  label="="  className="p-button rounded"  onClick={ () => props.onClick("=")}/>
                    <Button  label="/"  className="p-button rounded"  onClick={ () => props.onClick("/")}/>
                </div>
            </div>
        )
}

export default KeyPadComponent;