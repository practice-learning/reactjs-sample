import React, {useState} from 'react';
import {calculate, reset, blancspace} from "../methods/actions";

import KeyPadComponent from '../components/KeyPadComponent';
import ResultComponent from '../components/ResultComponent';

const Calculator = () => {
    
    const[result, setResult] = useState("");

    const onClick = button => {
        //setResult(button);
        if(button === "="){
            setResult(calculate(result));
        }
    
        else if(button === "C"){
            setResult(reset());
        }
    
        else if(button === "CE"){
            setResult(blancspace(result));
        }
    
        else{
            setResult(result + button);
        }
    }

    return(
        <div className="App-body">
        <h1>Calculator</h1>          
        <ResultComponent result={result}/>
        <KeyPadComponent onClick={(e) => onClick(e)}/>
      </div>        
    )
}

export default Calculator;