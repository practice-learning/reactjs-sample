import React, { useState } from 'react';

const Counter = ({ value }) => {
  // Declare a new state variable, which we'll call "count"
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
      <React.Fragment>
        <p>{value}</p>
      </React.Fragment>
    </div>
  );
}

export default Counter;