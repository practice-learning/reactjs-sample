
export const fetchMovies = async (complex) => {
  try {

    const response = await fetch(
      `http://localhost:8000/`,
      {
        method: "GET",
        mode: "cors",
        cache: "no-cache",
        headers: {
          "Content-Type": "application/json"
        }
      }
    );
    let res = await response.json();
    console.log(res);
    return res;

  } catch (err) {
    console.log("something")
    console.log(err.message);
    throw err;
  }
}